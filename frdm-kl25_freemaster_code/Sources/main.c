/*
 * Copyright (c) 2016, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// INCLUDES /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "MKL25Z4.h"                      // KL25 M0+ register definitions

// DEFINES //////////////////////////////////////////////////////////
//-------------------------------------------------------------------

// GLOBAL CONSTANTS /////////////////////////////////////////////////
//-------------------------------------------------------------------


// GLOBAL VARIABLES /////////////////////////////////////////////////
//-------------------------------------------------------------------
long touch,TouchThreshold,i;                 // touch variables
int LedSelect;

// FUNCTION HEADERS /////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void);                         // initializes MCU for Freedom Board

//---------------------------------------------------------------------
void RGB(int Red,int Green,int Blue);        // RGB-LED Control: 1=on, 0=off, for each of the 3 colors


// *** MAIN *********************************************************
//-------------------------------------------------------------------
int main(void)
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
i=0;                                         // reset TSI touch threshold counter to 0
TSI0_GENCS |= (  TSI_GENCS_MODE(0)           // Set to TSI Cap Sense (non-noise detection)
               | TSI_GENCS_REFCHRG(4)        // Reference osc charge/discharge current is 8uA
               | TSI_GENCS_DVOLT(0)          // Osc voltage rails set to 1.03V, Vp=1.33V, Vm=0.30V
               | TSI_GENCS_EXTCHRG(7)        // External electrode osc charge/discharge current is 64uA
               | TSI_GENCS_PS(4)             // Electrode osc prescaler is div/16
               | TSI_GENCS_NSCN(11)          // Number of scans for each external electrode is 18 times/electrode
              );

TSI0_GENCS |= TSI_GENCS_TSIEN_MASK;          // Enable TSI module

RGB(0,0,0);                                  // Start with all RGB LEDs off

for(;;)                                      // forever loop
 {
   TSI0_DATA |= TSI_DATA_TSICH(10);          // Select the TSI Channel 10 to be scanned
   TSI0_DATA |= TSI_DATA_SWTS_MASK;          // Start a software trigger to scan selected electrode(s)
   while (!(TSI0_GENCS&TSI_GENCS_EOSF_MASK));// Wait for electrode scan to complete
   touch=(TSI0_DATA&TSI_DATA_TSICNT_MASK);   // Read data from TSI0_Data register
   TSI0_GENCS |= TSI_GENCS_EOSF_MASK;        // Clear End of Scan Flag

   if (i<100)                                // Collect only 100 values for baseline
   {
     TouchThreshold = touch+50;              // Set Touch Threshold to 50 counts greater than baseline value
     i++;                                    // increment touch threshold counter
   }
   if (touch<TouchThreshold) RGB(0,0,0);     // Turn Red LED On when touched
   if (touch>TouchThreshold) RGB(0,1,0);     // Turn Green LED On when released

   switch (LedSelect)
    {
    	case 0:
              RGB(0,0,0);
              break;
    	case 1:
              RGB(1,0,0);
              break;
        case 2:
              RGB(0,1,0);
              break;
        case 3:
              RGB(0,0,1);
              break;
        case 4:
              RGB(1,1,0);
              break;
    	case 5:
              RGB(1,0,1);
              break;
        case 6:
              RGB(0,1,1);
              break;
        case 7:
              RGB(1,1,1);
              break;

    }

 }
// return 0;
}
//-------------------------------------------------------------------
// ******************************************************************


// FUNCTION BODIES //////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void)
{
// Crucial
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
SIM_COPC=0x00;                               // Disable COP watchdog

//System Registers
SIM_CLKDIV1 = 0x00;                          // Set Busclk to /1, Busclk=20,971,520Hz
SIM_SOPT2=0x05000000;                        // TPM Input Clock, UART0 Clock is MCGFLLCLK
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock (1 GPIO)
SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;             // Enable PortC clock
SIM_SCGC5|=SIM_SCGC5_PORTD_MASK;             // Enable PortD clock (2 GPIO)
SIM_SCGC5|=SIM_SCGC5_PORTE_MASK;             // Enable PortE clock
SIM_SCGC6|=SIM_SCGC6_PIT_MASK;               // Enable PIT module
SIM_SCGC6|=SIM_SCGC6_TPM1_MASK;              // Enable TPM2 module
SIM_SCGC6|=SIM_SCGC6_ADC0_MASK;              // Enable ADC module
SIM_SCGC4|=SIM_SCGC4_UART0_MASK;             // Enable UART0 module
SIM_SCGC4|=SIM_SCGC4_CMP_MASK;               // Enable CMP0 module
SIM_SCGC6|=SIM_SCGC6_DAC0_MASK;              // Enable DAC0 module
SIM_SCGC4|=SIM_SCGC4_I2C0_MASK;              // Enable I2C0 Module
SIM_SCGC5|=SIM_SCGC5_TSI_MASK;               // Enable TSI Module
SIM_SCGC7|=SIM_SCGC7_DMA_MASK;               // Enable DMA Module
SIM_SCGC6|=SIM_SCGC6_DMAMUX_MASK;            // Enable DMAMUX module

//System clock initialization
MCG_C1=0x04;                                 // FEI mode, internal clock 32KHz
MCG_C2=0x80;                                 // LOC reset enable, slow ref clock select

// GPIO Init
PORTB_PCR16=PORT_PCR_MUX(0);                 // Set PTB16 as TSI Channel 9
PORTB_PCR17=PORT_PCR_MUX(0);                 // Set PTB17 as TSI Channel 10
PORTB_PCR18=PORT_PCR_MUX(1);                 // Set Pin B18 to GPIO function
PORTB_PCR19=PORT_PCR_MUX(1);                 // Set Pin B19 to GPIO function
PORTD_PCR1=PORT_PCR_MUX(1);                  // Set Pin D1 to GPIO function
GPIOB_PDDR|=(1<<18);                         // Red LED, Negative Logic (0=on, 1=off)
GPIOB_PDDR|=(1<<19);                         // Green LED, Negative Logic (0=on, 1=off)
GPIOD_PDDR|=(1<<1);                          // Blue LED, Negative Logic (0=on, 1=off)
}
//---------------------------------------------------------------------
void RGB(int Red,int Green,int Blue)         // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<18); else GPIOB_PSOR|=(1<<18);
if (Green ==1) GPIOB_PCOR|=(1<<19); else GPIOB_PSOR|=(1<<19);
if (Blue  ==1) GPIOD_PCOR|=(1<<1);  else GPIOD_PSOR|=(1<<1);
}
//---------------------------------------------------------------------
long ADC_Read(int chan)                      // Analog-to-Digital Converter byte read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return ADC0_RA;}


// INTERRUPT BODIES /////////////////////////////////////////////////
//-------------------------------------------------------------------


